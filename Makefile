FTP=/var/ftp/pub/Etersoft/RX@Etersoft/Windows/
MAKENSIS=/usr/bin/makensis
CONFIG=nxclient.nsi
NXVER=3.3.0.3 3.3.0.6 3.4.0.7

define build-one
	for ver in $(NXVER) ; do $(MAKENSIS) -DBUILD=$(1) -DNXVER=$$ver $(CONFIG) || exit 1; done ;
endef

r11:
	$(call build-one,$@)
r12.1:
	$(call build-one,$@)
r12.2:
	$(call build-one,$@)
r12.3:
	$(call build-one,$@)
r12.4:
	$(call build-one,$@)
r12.5:
	$(call build-one,$@)
r12.6:
	$(call build-one,$@)
r12.7:
	$(call build-one,$@)
r12.8:
	$(call build-one,$@)
r12.9:
	$(call build-one,$@)
12.10:
	$(call build-one,$@)
r13.1:
	$(call build-one,$@)
r13.2:
	$(call build-one,$@)
r13.3:
	$(call build-one,$@)
r13.4:
	$(call build-one,$@)
r13.5:
	$(call build-one,$@)
r13.6:
	$(call build-one,$@)
r14.0:
	$(call build-one,$@)
r14.1:
	$(call build-one,$@)

all:
	$(foreach ver,$(subst nxwin-,,$(wildcard nxwin-*)),$(call build-one,$(ver)))

publish:
	cp -af release/* $(FTP)
