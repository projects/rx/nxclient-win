; The name of the installer
Name "NX Client Community Edition"
!ifndef BUILD
  !define BUILD r11
!endif
!ifndef NXVER
  !define NXVER 3.3.0.6
!endif
!ifndef DEST
  !define DEST ./release/${NXVER}
!endif

!include mime.nsh

; The file to write
!system 'mkdir -p ${DEST}'
OutFile "${DEST}/nxclient-${NXVER}-${BUILD}-win32.exe"

; The default installation directory
InstallDir $PROGRAMFILES\NXClientCE

; Registry key to check for directory (so if you install again, it will 
; overwrite the old one automatically)
InstallDirRegKey HKLM "Software\NXClientCE" "Install_Dir"

; Request application privileges for Windows Vista
RequestExecutionLevel admin

;--------------------------------

; Pages

Page components
Page directory
Page instfiles

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

; The stuff to install
Section "NXClient"

  SectionIn RO
  
  CreateDirectory $INSTDIR\bin
  CreateDirectory $INSTDIR\share
  CreateDirectory $INSTDIR\share\documents
  CreateDirectory $INSTDIR\share\documents\client
  CreateDirectory $INSTDIR\share\fonts
  CreateDirectory $INSTDIR\share\fonts\base
  CreateDirectory $INSTDIR\share\fonts\TTF
  CreateDirectory $INSTDIR\share\icons
  CreateDirectory $INSTDIR\share\images
  CreateDirectory $INSTDIR\share\keys

  SetOutPath $INSTDIR
  File "nxclient-${NXVER}/nxclient.exe"
  File "nxclient-${NXVER}/nxclient.url"

  SetOutPath $INSTDIR\bin
  File "nxclient-${NXVER}/bin/*"
  File "nxwin-${BUILD}/NXWin.exe"

  SetOutPath $INSTDIR\share
  File "nxclient-${NXVER}/share/*"

  SetOutPath $INSTDIR\share\documents\client
  File "nxclient-${NXVER}/share/documents/client/*"

  SetOutPath $INSTDIR\share\fonts\base
  File "nxclient-${NXVER}/share/fonts/base/*"

  SetOutPath $INSTDIR\share\fonts\TTF
  File "nxclient-${NXVER}/share/fonts/TTF/*"

  SetOutPath $INSTDIR\share\icons
  File "nxclient-${NXVER}/share/icons/*"

  SetOutPath $INSTDIR\share\images
  File "nxclient-${NXVER}/share/images/*"

  SetOutPath $INSTDIR\share\keys
  File "nxclient-${NXVER}/share/keys/*"

  SetOutPath ""
  ; Write the installation path into the registry
  WriteRegStr HKLM SOFTWARE\NXClientCE "Install_Dir" "$INSTDIR"
  
  ; Write the uninstall keys for Windows
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\NXClientCE" "DisplayName" "NX Client Community Edition"
  WriteRegStr HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\NXClientCE" "UninstallString" '"$INSTDIR\uninstall.exe"'
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\NXClientCE" "NoModify" 1
  WriteRegDWORD HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\NXClientCE" "NoRepair" 1

  WriteRegStr HKCR ".nxs" "" "NXClientCE.session"
  WriteRegStr HKCR "NXClientCE.session" "" "NX Client for Windows CE session file"
  WriteRegStr HKCR "NXClientCE.session\DefaultIcon" "" "$INSTDIR\share\icons\nxclient-desktop.ico"
  WriteRegStr HKCR "NXClientCE.session\shell" "" "open"
  WriteRegStr HKCR "NXClientCE.session\shell\open\command" "" '$INSTDIR\nxclient.exe --session "%1"'

  WriteRegStr HKCR "Applications\nxclient.exe" "" ""
  WriteRegStr HKCR "Applications\nxclient.exe\shell" "" "open"
  WriteRegStr HKCR "Applications\nxclient.exe\shell\open\command" "" '$INSTDIR\nxclient.exe --session "%1"'

  !insertmacro UPDATEFILEASSOC

  WriteUninstaller "uninstall.exe"
  
SectionEnd

; Optional section (can be disabled by the user)
Section "Start Menu Shortcuts"

  CreateDirectory "$SMPROGRAMS\NX Client Community Edition"
  CreateShortCut "$SMPROGRAMS\NX Client Community Edition\NX Client Community Edition.lnk" "$INSTDIR\nxclient.exe" "" "$INSTDIR\share\icons\nxclient-desktop.ico" 0
  CreateShortCut "$SMPROGRAMS\NX Client Community Edition\NX Connection Wizard.lnk" "$INSTDIR\nxclient.exe" "--wizard" "$INSTDIR\share\icons\nxclient-wizard.ico" 0
  CreateShortCut "$SMPROGRAMS\NX Client Community Edition\NX Session Administrator.lnk" "$INSTDIR\nxclient.exe" "--admin" "$INSTDIR\share\icons\nxclient-admin.ico" 0
  CreateShortCut "$SMPROGRAMS\NX Client Community Edition\NX Help on the Web.lnk" "$INSTDIR\nxclient.url"
  CreateShortCut "$SMPROGRAMS\NX Client Community Edition\Uninstall.lnk" "$INSTDIR\uninstall.exe" "" "$INSTDIR\share\icons\nxclient-uninstall.ico" 0

SectionEnd

Section "Desktop Shotcut"

  CreateShortCut "$DESKTOP\NX Client Community Edition.lnk" "$INSTDIR\nxclient.exe" "" "$INSTDIR\share\icons\nxclient-desktop.ico" 0

SectionEnd

;--------------------------------

; Uninstaller

Section "Uninstall"
  
  ; Remove registry keys
  DeleteRegKey HKLM "Software\Microsoft\Windows\CurrentVersion\Uninstall\NXClientCE"
  DeleteRegKey HKLM SOFTWARE\NXClientCE
  DeleteRegKey HKCR "NXClientCE.session"
  DeleteRegKey HKCR ".nxs"

  ; Remove files and uninstaller
  Delete $INSTDIR\share\documents\client\*
  Delete $INSTDIR\share\documents\*
  Delete $INSTDIR\share\fonts\base\*
  Delete $INSTDIR\share\fonts\TTF\*
  Delete $INSTDIR\share\fonts\*
  Delete $INSTDIR\share\icons\*
  Delete $INSTDIR\share\images\*
  Delete $INSTDIR\share\keys\*
  Delete $INSTDIR\bin\*
  Delete $INSTDIR\share\*
  Delete $INSTDIR\nxclient.exe
  Delete $INSTDIR\nxclient.url
  Delete $INSTDIR\uninstall.exe

  ; Remove shortcuts, if any
  Delete "$SMPROGRAMS\NX Client Community Edition\*.*"
  Delete "$DESKTOP\NX Client Community Edition.lnk"

  ; Remove directories used
  RMDir "$SMPROGRAMS\NX Client Community Edition"
  RMDir "$INSTDIR\share\documents\client"
  RMDir "$INSTDIR\share\documents"
  RMDir "$INSTDIR\share\fonts\base"
  RMDir "$INSTDIR\share\fonts\TTF"
  RMDir "$INSTDIR\share\fonts"
  RMDir "$INSTDIR\share\icons"
  RMDir "$INSTDIR\share\images"
  RMDir "$INSTDIR\share\keys"
  RMDir "$INSTDIR\share"
  RMDir "$INSTDIR\bin"
  RMDir "$INSTDIR"

SectionEnd
